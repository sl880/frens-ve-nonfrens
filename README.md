# Koliedzy vs niekoliedzy
--version 0.0.0  
(2020)

## Instrukcja gry

#### Wprowadzenie
Gierka przypomina zabawą "kamień, papier nożyce" z małą modyfikacją.  
Polega ona na tym, że prawodpodobieństwo porażki w każdej bitwie  
"papier, kamień, nożyce" to 1/3, a wygranej 1/3.  
  
W "Koliedzy vs niekoliedzy" prawdopodobieństwo porażki to 2/3 a wygranej 1/3.  


#### Zasady walki
Pepe z pieniążkami pokonuje Polkę, bo ona traci wtedy kontrolę na sobą  
 i leci na jego kasę, a on ją wtedy hyc w ryja z nienacka z piąchy.  
  
Pepe z sierpem i młotem pokonuje lewaka. Najpierw zadaje obrażenia obuchowe  
młotem, lewak zostaje ogłoszony i Pepe rozrywa jego pseudo tęczowe "flagi"  
sierpem.  

Na Chada to nie ma mocnych, ciężko go załatwić, więc tu daje radę tylko  
Pepe komandos, szkolony od dziecka by bronić tag przed najazdem normickich  
Chadów.  